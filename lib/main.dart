import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rainbow_colors/model/cat.dart';
import 'package:rainbow_colors/provider/color_theme.dart';
import 'package:http/http.dart' as http;

import 'model/rainbow_color.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ColorProvider>(
            create: (context) => ColorProvider())
      ],
      child: Consumer<ColorProvider>(
        builder: (context, model, child) => MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: model.color,
          ),
          home: const MyHomePage(title: 'Flutter Demo Home Page'),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: [
            FutureBuilder<http.Response>(
              future: http.get(Uri.parse("https://cataas.com/cat?json=true")),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicator();
                }
                if (snapshot.hasError) {
                  return Text("Não foi possível localizar o gatinho.");
                }
                if (snapshot.hasData) {
                  Cat gatinho = Cat.fromJson(jsonDecode(snapshot.data!.body));
                  return Column(
                    children: [
                      Text(
                        "O id do gatinho eh ${gatinho.id}",
                      ),
                      Image.network("https://cataas.com/${gatinho.url}")
                    ],
                  );
                }
                return Text("Ainda nao.");
              },
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class RainbowColor {
  int id;
  String name;
  String weight;

  RainbowColor({required this.id, required this.name, required this.weight});

  RainbowColor.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        id = json['id'],
        weight = json['weight'];

  Map<String, dynamic> toJson() => {'name': name, 'id': id, 'weight': weight};
}
